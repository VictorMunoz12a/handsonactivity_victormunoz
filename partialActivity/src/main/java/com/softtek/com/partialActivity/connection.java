package com.softtek.com.partialActivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mysql.cj.xdevapi.Statement;

public class connection {
	private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    List<Devices> list = new ArrayList<Devices>();
    List<Devices> lista = new ArrayList<Devices>();

	public void readDataBase() throws Exception {
		System.out.println("MySQL JDBC Connection");
		List<Devices> result=new ArrayList<>();
       queries qu=new queries();
        	 try (Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234")) {
                 if (conn != null) {
                     System.out.println("Connected to the database!");
                     PreparedStatement preparedStatement = conn.prepareStatement(qu.Queries("Select * from Device"));
                     ResultSet resultSet = preparedStatement.executeQuery();
                     while (resultSet.next()) {                		
                    	 int DeviceId=resultSet.getInt("DeviceID");
                    	 String Name=resultSet.getString("Name");
                    	 String Description=resultSet.getString("Description");
                    	 int ManufacturerID=resultSet.getInt("ManufacturerID");
                    	 int ColorId=resultSet.getInt("ColorID");
                    	 String Comments=resultSet.getString("Comments");
                    	 Devices dev=new Devices(DeviceId,Name,Description, ManufacturerID,ColorId,Comments);
                         
              
                      
                         System.out.println("ID: " + DeviceId + " Device's name: " + Name + " Description: " + Description+" Manufacturer ID:"+ ManufacturerID+" Color´s ID: "+ColorId+" Comments: "+Comments);
                         long count=result.stream()//
                                 .map(Devices::getManufacturerID)
                                 .filter(m->m==3).count();
                                 System.out.println("Conteo en "+count);    
                                 list=result.stream()
                                     .filter(m->m.getColorId()==1)
                                         .collect(Collectors.toList());                           
                                 lista.stream().map(Devices::getName).forEach(System.out::println);
                                   Map<Integer,Devices> map=result.stream()
                                              .collect(
                                                      Collectors.toMap(
                                                              Devices::getDeviceId,Function.identity())
                                                      );
                                    map.forEach((k,v)->System.out.println("Llave: "+k+" valor: "+v.getName()));
                                    
                                    List<String> devices = result.stream()
                                            .map(Devices::getName)
                                            .filter(m -> m.startsWith("S"))
                                            .collect(Collectors.toList());
                                            devices.forEach(System.out::println);
                                            
                                            Map<Integer, String> maps = new HashMap<>();
                                            maps.forEach((k,v) -> {
                                                System.out.println("LLave: " + k + " Valor: " + v);            
                                                
                                            });
                     }
                     
                    
                 } else {
                     System.out.println("Failed to make connection!");
                 }
                 result.forEach(System.out::println);
             } catch (SQLException e) {
                 System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
             } catch (Exception e) {
                 e.printStackTrace();
             }   
        
}
	 private void close() {
         try {
             if (resultSet != null) {
                 resultSet.close();
             }

             if (statement != null) {
                 ((Connection) statement).close();
             }

             if (connect != null) {
                 connect.close();
             }
         } catch (Exception e) {

         }
     }
	  public static <Devices> void getStream(List<Devices> list) 
	    { 
	  
	        // Create stream object with the List 
	        Stream<Devices> stream = list.stream(); 
	  
	        // Iterate list first to last element 
	        Iterator<Devices> it = stream.iterator(); 
	  
	        // Iterate stream object 
	        while (it.hasNext()) { 
	            System.out.print(it.next() + " "); 
	        } 
	    } 
	 
	
	 
}
